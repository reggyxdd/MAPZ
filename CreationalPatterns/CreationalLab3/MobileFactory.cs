﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    class MobileFactory : AbstractFactory
    {
        public WindowCancel CreateWindowCamcel()
        {
            return new WindowCancelMobile();
        }

        public WindowRecord CreateWindowRecord()
        {
            return new WindowRecordMobile();
        }
    }
}
