﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    interface IClone
    {
        IClone Clone();
    }
}
