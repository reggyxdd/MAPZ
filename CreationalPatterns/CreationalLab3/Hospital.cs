﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    class Hospital
    {
        private string name;
        private static Hospital instance;
        private Hospital(string name) 
        {
            this.name = name;
        }
       //Singleton
        public static Hospital GetInstance(string name)
        {
            if (instance == null)
                instance = new Hospital(name);
            else
            {
                instance.name = name;
                throw new Exception("You can't add second instance.");
            }
            return instance;
        }
      
        public string Name { get => name; set => name = value; }
    }
}
