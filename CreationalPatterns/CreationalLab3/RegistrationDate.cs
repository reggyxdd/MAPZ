﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    class RegistrationDate : IClone
    {
        private string data;
        public RegistrationDate(string data)
        {
            this.data = data;
        }
        public IClone Clone()
        {
            return new RegistrationDate(data);
        }
        public override string ToString()
        {
            return data;
        }
    }
}
