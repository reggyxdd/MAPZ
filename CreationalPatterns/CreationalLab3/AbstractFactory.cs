﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    interface AbstractFactory
    {
        WindowRecord CreateWindowRecord();
        WindowCancel CreateWindowCamcel();
    }
}
