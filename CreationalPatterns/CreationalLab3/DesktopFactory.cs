﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    class DesktopFactory : AbstractFactory
    {
        public WindowCancel CreateWindowCamcel()
        {
            return new WindowCancelDesktop();
        }

        public WindowRecord CreateWindowRecord()
        {
            return new WindowRecordDesktop();
        }
    }
}
