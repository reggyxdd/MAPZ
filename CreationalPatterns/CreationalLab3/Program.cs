﻿using System;

namespace CreationalLab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" Singleton");

            Hospital hospital = Hospital.GetInstance("Ihor Hrynyk no allergy");
            Console.WriteLine(hospital.Name);
            try
            {
                Hospital hospital1 = Hospital.GetInstance("Oleh Voznyi no allergy");
                Console.WriteLine(hospital1.Name);
                Console.WriteLine(object.Equals(hospital, hospital1));
            }
            catch( Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("\n Abstract Factory");

            AbstractFactory abstractFactory = new DesktopFactory();
            WindowRecord windowRecord = abstractFactory.CreateWindowRecord();
            Console.WriteLine(windowRecord);
            abstractFactory = new MobileFactory();
            WindowRecord windowRecord1 = abstractFactory.CreateWindowRecord();
            Console.WriteLine(windowRecord1);

            Console.WriteLine("\n Prototype");
            IClone record = new Record("Anatoliy Petrasyuk no allergy", new RegistrationDate("02.06.2020"));
            IClone recordCopy = record.Clone();
            Console.WriteLine(record.ToString());
            Console.WriteLine(recordCopy.ToString());
            
        }
    }
}
