﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    class WindowRecordDesktop: WindowRecord
    {
        public override string ToString()
        {
            return base.ToString() + " application for desktop devices";
        }
    }
}
