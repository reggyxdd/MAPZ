﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    class Record: IClone
    {
        private string patient;
        private RegistrationDate registrationDate;
        public Record(string p, RegistrationDate reg)
        {
            patient = p;
            registrationDate = reg;
        }
        public string Patient { get => patient; set => patient = value; }
        internal RegistrationDate RegistrationDate { get => registrationDate; set => registrationDate = value; }

        public IClone Clone()
        {
            return new Record(patient, (RegistrationDate)registrationDate.Clone());
        }
        public override string ToString()
        {
            return "Patient: " + patient + ". with Registration date: " + registrationDate;
        }
    }
}
