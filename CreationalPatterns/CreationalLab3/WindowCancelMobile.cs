﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreationalLab3
{
    class WindowCancelMobile: WindowCancel
    {
        public override string ToString()
        {
            return base.ToString() + " application for mobile devices";
        }
    }
}
