﻿using System;
using System.Collections.Generic;

namespace Struct
{
    class Program
    {
        static void Main(string[] args)
        {

            // Facade
            Console.WriteLine("Facade:");
            Records recordSFacade = new Records();
            recordSFacade.AddRecord(new Record("Eren Jeager", "People"));
            recordSFacade.AddRecord(new Record("Mikasa Ackerman", "No"));
            FacadeRecord actionRecord = new FacadeRecord(recordSFacade);
            string patient = "Levi Ackerman";
            string Allergy = "Monkeys";
            FacadeRecords actionsWithRecordsFacade = new FacadeRecords(actionRecord, patient, Allergy);
            actionsWithRecordsFacade.AddRecord();
            Console.WriteLine();

            // Decorator
            Console.WriteLine("Decorator:");
            IComponent record = new Record("Yurii Hreschuk", "Flowers");
            Console.WriteLine("Record without using decorator:  \n" + record);
            IComponent recordDecorated = new OutputDecoratorRecord(record);
            Console.WriteLine("Record with decorator:\n" + recordDecorated);
            Console.WriteLine();
            record = new Record("Ivan Yuriev", "No");
            Console.WriteLine("Record without using decorator:  \n" + record);
            IComponent ExtensionRecipes = new ExtensionRecipe(record, "2");
            Console.WriteLine("Record with extension of the recipe:  "+ ExtensionRecipes);
            Console.WriteLine();

            ExtensionRecipes = new OutputDecoratorRecord(ExtensionRecipes);
            Console.WriteLine("Record with decorator:\n"+ ExtensionRecipes);
            Console.WriteLine();
          
            // Proxy
            Console.WriteLine("Proxy: ");
            Console.WriteLine("Record without proxy: ");
            Record record1 = new Record("Andriy Petrenko", "Lactose", new List<string>() { "Milk", "Cheese", "Yogurt" });
            ISubjectRecord subject = record1;
            subject.IncludeElements(); 
            Console.WriteLine("Record using proxy: ");
            subject = new ProxyRecord(record1);
            subject.IncludeElements();



        }

    }
}
