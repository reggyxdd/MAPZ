﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Struct
{
    class FacadeRecord
    {
        private Records records;
        public FacadeRecord(Records records)
        {
            this.records = records;
        }

        public void Create(string patient, string regDate)
        {
            Record record = new Record(patient, regDate);
            records.AddRecord(record);
        }
        public void ShowAll()
        {
            records.Show();
        }
    }
}
