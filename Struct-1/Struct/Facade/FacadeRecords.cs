﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Struct
{
    class FacadeRecords
    {
        private FacadeRecord actionRecord;
        private string Patient;
        private string Allergy;
        public FacadeRecords(FacadeRecord actionRecord, string patient, string Allergy)
        {
            this.actionRecord = actionRecord;
        
            this.Patient = patient;
            this.Allergy = Allergy;

        }

        public void AddRecord()
        {
            actionRecord.Create(Patient, Allergy);
            actionRecord.ShowAll();
        }
        

    }
}
