﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Struct
{
    class OutputDecoratorRecord: Decorator
    {

        public OutputDecoratorRecord(IComponent component): base(component)
        {    
        }
        public override string ToString()
        {
            
            return "\n" +"<<" + component.ToString().ToUpper() + ">>" + "\n" ;  
        }
        public string BaseToString()
        {
            return component.ToString();
        }
       
    }
}
