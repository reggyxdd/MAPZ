﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Struct
{
    abstract class Decorator: IComponent
    {
        protected IComponent component;
        abstract public override string ToString();
        public Decorator(IComponent component = null)
        {
            this.component = component;
        }
        public void SetComponent(IComponent component)
        {
            this.component = component;
        }
       
    } 
}
