﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Struct
{
    class Records
    {
        private List<Record> PatientRecords;

        public Records()
        {
            PatientRecords = new List<Record>();
        }

        internal List<Record> _Records { get => PatientRecords; set => PatientRecords = value; }
        public virtual void ModifyNew()
        {
            Console.WriteLine("[]");
        }
        public void AddRecord(Record record)
        {
            PatientRecords.Add(record);
        }
        public void Show()
        {
            foreach(Record r in PatientRecords)
            {
                Console.WriteLine(r);
            }
        }
        public void Reschedule(Record record, string date)
        {
            PatientRecords.Find((r) => { return r.Patient == record.Patient; } ).RegistrationDate = date;
        }

    }
}
