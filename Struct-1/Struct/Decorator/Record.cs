﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Struct
{
    class Record: IComponent, ISubjectRecord
    {
        private string patient;
        private string allergy;
        private List<string> listOfAttends;
        #region
        public Record(string p, string reg)
        {
            patient = p;
            allergy = reg;
            this.ListOfAttends = new List<string>();
        }
        public Record(string p, string reg, List<string> list)
        {
            patient = p;
            allergy = reg;
            this.ListOfAttends = list;
        }
        public string Patient { get => patient; set => patient = value; }
        public List<string> ListOfAttends { get => listOfAttends; set => listOfAttends = value; }
        internal string RegistrationDate { get => allergy; set => allergy = value; }
        #endregion
        public void Inclusions(string attand)
        {
            this.ListOfAttends.Add(attand);
        }

        public void IncludeElements()
        {
            string inclusionList = "";
            foreach(string newInclusion in this.ListOfAttends)
            {
                inclusionList += newInclusion + "\n";
            }
            Console.WriteLine(this.ToString() + ", Including: " + inclusionList);
        }

        public override string ToString()
        {
            return "Patient: " + patient + ". Allergy: " + allergy;
        }
    }
}
