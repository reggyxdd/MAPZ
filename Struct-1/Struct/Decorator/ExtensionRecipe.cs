﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Struct
{
    class ExtensionRecipe: Decorator
    {
        private string recipeExtension;

        public string RescheduleDate { get => recipeExtension; set => recipeExtension = value; }
        public ExtensionRecipe(IComponent component, string rescheduleDate): base(component)
        {
            this.recipeExtension = rescheduleDate;
        }

        public override string ToString()
        {
            return component.ToString() + ". Extension of the recipe: "+recipeExtension;
        }
    }
}
