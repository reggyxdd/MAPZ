﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Struct
{
    class ProxyRecord: ISubjectRecord
    {
        private Record record;

        public ProxyRecord(Record record)
        {
            this.record = record;
        }

        public void IncludeElements()
        {
            Console.WriteLine(record.ToString());
        }
    }
}
