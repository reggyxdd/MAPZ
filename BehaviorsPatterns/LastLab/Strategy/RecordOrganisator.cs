﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class RecordOrganisator
    {
        private IStrategy strategy;
        private Records records;
        public RecordOrganisator(IStrategy strategy, Records records)
        {
            this.Strategy = strategy;
            this.records = records;
        }

        internal IStrategy Strategy { set => strategy = value; }

        public void SetRecord()
        {
            Record record = new Record("Oleh Zasidkovich", "No");
            if (strategy is MakeRecord)
            {
                strategy.ChangeAllergy(records, record);
            }
            else if (strategy is NewTypeAllergy)
            {
                Arguments arguments = new Arguments(record, "Flowers");
                strategy.ChangeAllergy(records, arguments);
            }
            else if (strategy is ViewRecord)
            {
                strategy.ChangeAllergy(records, record);
            }
        }
    }
}
