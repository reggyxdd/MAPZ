﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class ViewRecord : IStrategy
    {
        public void ChangeAllergy(object objectCollection, object argument)
        {
            (objectCollection as Records).DisplayRecords();
            Console.WriteLine("Success using strategy pattern");
        }
    }
}
