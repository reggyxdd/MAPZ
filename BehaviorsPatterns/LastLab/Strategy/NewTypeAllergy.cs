﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class NewTypeAllergy : IStrategy
    {
        public void ChangeAllergy(Object objCollecton, Object object1)
        {
            (objCollecton as Records).ChangeAllergy((object1 as Arguments).record, (object1 as Arguments).Allergy) ;
            Console.WriteLine("Record with new type of allergy");
        }
    }
}
