﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class Record
    {
        private string patient;
        private string Allergy;
        public Record(string p, string reg)
        {
            patient = p;
            Allergy = reg;
        }
        public string Patient { get => patient; set => patient = value; }
        internal string AllergyType { get => Allergy; set => Allergy = value; }

        public override string ToString()
        {
            return "Patient: " + patient + ". Allergy: " + Allergy;
        }
    }
}
