﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class Records
    {
        private List<Record> AllRecords = new List<Record>();
      
        public virtual void ModifyNew()
        {
            Console.WriteLine("This is ModifyNew Method");
        }
        public void AddRecord(Record record)
        {
            AllRecords.Add(record);
        }
        public void ChangeAllergy(Record record1, string newAllergy)
        {
            int index = AllRecords.FindIndex((r) => { return r.Patient == record1.Patient; });
            AllRecords[index].AllergyType = newAllergy;
        }
        public void DisplayRecords()
        {
            foreach(Record record in AllRecords)
            {
                Console.WriteLine(record);
            }
        }

    }
}
