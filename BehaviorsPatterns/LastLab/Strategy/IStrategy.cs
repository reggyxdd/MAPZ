﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    interface IStrategy
    {
        void ChangeAllergy(Object objectCollection, Object argument);
    }
}
