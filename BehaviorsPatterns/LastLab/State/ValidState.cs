﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class ValidState : StateOfRecord
    {
        public override void Handle(PatientContext patientContext, Records records, string name)
        {
            Console.WriteLine("Enter what is the patient allergic to? " + name + ".");
            string Allergy = Console.ReadLine();
            Record record = new Record(name, Allergy);
            records.AddRecord(record);
            Console.WriteLine("You added new record about: " + record.ToString());
        }
    }
}
