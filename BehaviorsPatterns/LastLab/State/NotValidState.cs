﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class NotValidState : StateOfRecord
    {
        public override void Handle(PatientContext patientContext, Records records, string name)
        {
            Console.WriteLine("Patient's record is inActive now. Check it out to activate.");
        }
    }
}
