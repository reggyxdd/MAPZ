﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class PatientContext
    {
        private string patientName;
        private StateOfRecord stateOfRecord;

        public PatientContext(StateOfRecord stateOfRecord, string patientName)
        {
            this.stateOfRecord = stateOfRecord;
            this.patientName = patientName;
        }
        internal StateOfRecord StateOfRecord { get => stateOfRecord;}
        public void Request(Records records)
        {
            this.stateOfRecord.Handle(this, records, patientName); 
        }
    }
}
