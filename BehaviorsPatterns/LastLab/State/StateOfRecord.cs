﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    abstract class StateOfRecord
    {
        public abstract void Handle(PatientContext patientContext, Records records, string name);
    }
}
