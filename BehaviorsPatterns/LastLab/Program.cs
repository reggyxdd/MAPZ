﻿using System;

namespace LastLab
{
    class Program
    {

        static void Main(string[] args)
        {

            // State 
            Console.WriteLine("State: ");
            Console.WriteLine("Enter patient name to show info: ");
            Records records = new Records();
            string Name = Console.ReadLine();
            PatientContext context = new PatientContext(new ValidState(), Name);
            context.Request(records);
            if (context.StateOfRecord is ValidState)
            {
                Console.WriteLine("Record about: \'" + Name + "\' is valid");
            }
            else
                Console.WriteLine("Record about: \'" + Name + "\' isn't valid");

            // Chain of responsibility
            Console.WriteLine("Chain of responsibility: ");
            Console.WriteLine("Enter whether the medication causes side effects?: ");
            bool SideEffects = Convert.ToBoolean(Console.ReadLine());
            ValidatorHandle validator1 = new NameHandler();
            ValidatorHandle validator2 = new AllergyHandler();
            ValidatorHandle validator3 = new SideEffectsHandler(SideEffects);
            validator1.Next = validator2;
            validator2.Next = validator3;
            validator1.HandleRequest(new Record("Anton", "04.09.2021"));

            // Strategy
            Console.WriteLine("Strategy: ");
            IStrategy strategy = new MakeRecord();
            RecordOrganisator organisator = new RecordOrganisator(strategy, records);
            organisator.SetRecord();
            records.DisplayRecords();
            
            organisator.Strategy = new NewTypeAllergy();
            organisator.SetRecord();
            records.DisplayRecords();

            organisator.Strategy = new ViewRecord();
            organisator.SetRecord()
            records.DisplayRecords();

        }
    }
}
