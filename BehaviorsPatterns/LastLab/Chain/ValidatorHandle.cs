﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    abstract class ValidatorHandle
    {
        protected ValidatorHandle next;
        protected bool isValid;

        protected ValidatorHandle()
        {
            this.next = null;
            this.isValid = false;
        }

        public bool IsValid { get => isValid; }
        internal ValidatorHandle Next {set => next = value; }
        public abstract void HandleRequest(Record record);

    }
}
