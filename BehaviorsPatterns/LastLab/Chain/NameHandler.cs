﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class NameHandler : ValidatorHandle
    {
        public NameHandler(): base()
        {
        }

        public override void HandleRequest(Record record)
        {
            this.isValid = true;
            for (int i = 0; i < record.Patient.Length; i++)
            {
                if (!char.IsLetter(record.Patient[i]))
                {
                    this.isValid = false;
                    break;
                }
            }
            if(IsValid)
            {
                Console.WriteLine("Name validation: success.");
                this.next.HandleRequest(record);
            }
        }
    }
}
