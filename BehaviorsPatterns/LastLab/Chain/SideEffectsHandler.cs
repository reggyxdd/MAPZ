﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class SideEffectsHandler : ValidatorHandle
    {
        private bool sideEffects;

        public SideEffectsHandler(bool sideEffects): base()
        {
            this.sideEffects = sideEffects;
        }

        public override void HandleRequest(Record record)
        {
            if (!sideEffects)
            {
                Console.WriteLine("The drugs have no side effects.");
            }
            else
            {
                Console.WriteLine("The drugs have some side effects");
            }
            this.next = null;
           

        }

        
    }
}
