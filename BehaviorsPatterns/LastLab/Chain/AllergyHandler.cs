﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LastLab
{
    class AllergyHandler : ValidatorHandle
    {
        public AllergyHandler() : base()
        {
        }
        public override void HandleRequest(Record record)
        {
            Console.WriteLine("Allergy validation: success");
            this.next.HandleRequest(record);

        }
    }
}
